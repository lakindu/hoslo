<?php


namespace App\BO\Agent\v100\Repositories\Interfaces;
 
use Illuminate\Http\Request;

interface AgentRepositoryInterface
{
    public function login(Request $request);

    public function register(Request $request);
}
