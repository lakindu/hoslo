<?php
/**
 * LocationRepository
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev3
 * Date: 20/01/2020
 * Time: 10:34 AM
 */

namespace App\BO\Agent\v100\Repositories;

use App\Base\Exception\BaseException;
use App\BO\Agent\v100\Models\Agent; 
use App\BO\Agent\v100\Repositories\Interfaces\AgentRepositoryInterface;
use App\BO\Agent\v100\Models\AgentProductGroup;
use App\BO\Company\v100\Models\Company;
use App\BO\PushNotification\v100\Services\PushNotificationService;
use App\Helpers\OTP\OTPGenerator;
use App\Helpers\OTP\OTPModel;
use App\Mail\v100\AgentRegisterOTP;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Notifications\Agent as AgentNotification;
use Illuminate\Support\Facades\Config;

class AgentRepository implements AgentRepositoryInterface
{
    protected $agentModel = null;
    protected $oTpModel = null;
    protected $pushNoticationService;


    public function __construct(Agent $agent, OTPModel $oTPModel, PushNotificationService $pushNoticationService)
    {
        $this->agentModel = $agent;
        $this->oTpModel = $oTPModel;
        $this->pushNoticationService = $pushNoticationService;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function login(Request $request)
    {
        try {
            $user = $this->agentModel
                ->where('phone', $request->phone)
                ->orderBy('id', 'DESC')
                ->first();

            if ($user) { 
                return $user;
            } else {
                throw new BaseException(9001);
            }
        } catch (BaseException $e) {
            throw $e;
        } catch (\Exception $e) {             
            throw new BaseException(9001);
        }

    }
 
    public function updateAgentBasicInfo(array $data, int $id = 0)
    {
        try {
            $suppliers = isset($data['suppliers']) ? $data['suppliers'] : [];

            unset($data['current_password']);
            unset($data['password_confirmation']);
            unset($data['suppliers']);
            unset($data['hidden']);
            Agent::where('id', '=', $id)->update($data);
            $agent = Agent::find($id); 
            return Agent::find($id);
        } catch (\Exception $e) {
            throw new BaseException(9005, ['message' => $e->getMessage()]);
        }
    }

    public function updateSocialBasicInfo($data, int $id = 0)
    {
        try {

            $data = [
                "phone" => $data->phone,
                "gender" => $data->gender,
                "address_1" => $data->address_1,
                "address_2" => $data->address_2,
                "city" => $data->city,
            ];

            Agent::where('id', $id)->update($data);

            return Agent::find($id);
        } catch (\Exception $e) {
            throw new BaseException(9005, ['message' => $e->getMessage()]);
        }
    }

    public function getCustomers()
    {
        try {
            $agents = Agent::where('is_verified', 1)->select(DB::raw('CONCAT(name," - ",phone) as text'), 'id')->get();
            $agents = $agents->pluck('text', 'id');
            return $agents;
        } catch (QueryException $e) {
            return false;
        }
    }
}
