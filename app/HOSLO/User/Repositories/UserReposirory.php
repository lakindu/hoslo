<?php
/**
 * LocationRepository
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev3
 * Date: 20/01/2020
 * Time: 10:34 AM
 */

namespace App\HOSLO\User\Repositories;

use App\Base\Exception\BaseException;
use App\HOSLO\User\Models\Academic;
use App\HOSLO\User\Models\Institution;
use App\HOSLO\User\Models\User;
use App\HOSLO\User\Repositories\Interfaces\UserRepositoryInterface;
use App\BO\Agent\v100\Models\AgentProductGroup;
use App\BO\Company\v100\Models\Company;
use App\BO\PushNotification\v100\Services\PushNotificationService;
use App\Helpers\OTP\OTPGenerator;
use App\Helpers\OTP\OTPModel;
use App\Mail\v100\AgentRegisterOTP;
// use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Auth;
use App\Notifications\Agent as AgentNotification;
use Illuminate\Support\Facades\Config;

class UserReposirory implements UserRepositoryInterface
{
    protected $userModel = null;
    protected $instituteModel = null;
    protected $pushNoticationService;
    protected $academic = null;


    public function __construct(User $user,Institution $institute, Academic $academic)
    {
        $this->userModel = $user;
        $this->academicModel=$academic;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */


    public function getUser()
    {
        try {
//
//             return $institutes = Academic::with('instituteDetails')->get();
            $users = User::with('acadamicDetails','academic.instituteDetails')->get();
            return $users;

        } catch (QueryException $e) {
            return false;
        }
    }

    public function createUser($request){
        $user = new User;

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->username = $request->username;
        $user->password = $request->password;
        $user->is_admin = $request->is_admin;
        $user->created_at = $request->created_at;
        $user->updated_at = $request->updated_at;

        return $user->save();
    }

    public function updateUser($request,$id){
        $user =  User::find($id);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->username = $request->username;
        $user->password = $request->password;
        $user->is_admin = $request->is_admin;
        $user->created_at = $request->created_at;
        $user->updated_at = $request->updated_at;

        return $user->update();
    }

    public function deleteUser($id){
        $user = User::find($id);
       return $user->delete();
    }
}
