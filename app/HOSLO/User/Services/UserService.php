<?php
/**
 * LocationService
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev4
 * @Date       2020-01-20
 * @Time       4:13 PM
 */

namespace App\HOSLO\User\Services;

use App\Base\Exception\BaseException;
use App\Base\Services\BaseService;
use App\HOSLO\User\Models\User;
use App\HOSLO\User\Repositories\UserReposirory;
use App\HOSLO\User\Transformations\UserTransformable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserService extends BaseService
{
    protected $userRepo;
    protected $websiteUserRepo;

    use UserTransformable;

    public function __construct(UserReposirory $userRepo)
    {
        $this->userRepo = $userRepo;
        parent::__construct();
    }

    public function getUser()
    {
        try {
            $agent = $this->userRepo->getUser();
            return $agent;
        } catch (BaseException $e) {
            throw $e;
        }
    }
    public function createUser($request)
    {

        try {
            $user = $this->userRepo->createUser($request);
            return $user;
        } catch (BaseException $e) {
            throw $e;
        }
    }

    public function updateUser($request,$id)
    {
        try {
            $user = $this->userRepo->updateUser($request,$id);
            return $user;
        } catch (BaseException $e) {
            throw $e;
        }
    }

    public function deleteUser($id){
        try {
            $user = $this->userRepo->deleteUser($id);
            return $user;
        } catch (BaseException $e) {
            throw $e;
        }
    }
}
