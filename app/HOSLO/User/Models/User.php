<?php

namespace App\HOSLO\User\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = "users";

    protected $primaryKey = "id";
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'address',
        'username',
        'password',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

     public function acadamicDetails()
     {
         return $this->hasMany(Academic::class,'user_id', 'id');
     }
}
