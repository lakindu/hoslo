<?php

namespace App\HOSLO\User\Models;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    protected $table = "institutions";

    protected $primaryKey = "id";
    protected $fillable = [
        'institute',
        'logo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
