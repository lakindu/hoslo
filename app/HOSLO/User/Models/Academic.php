<?php

namespace App\HOSLO\User\Models;


use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    protected $table = "academics";
    protected $primaryKey = "id";
    protected $fillable = [
        'qualifications'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function instituteDetails()
    {
        return $this->belongsTo(Institution::class,'id');
    }
}
