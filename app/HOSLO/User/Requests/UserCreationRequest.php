<?php


namespace App\HOSLO\User\Requests;
use App\Base\Requests\BaseRequest;
use Config;

class UserCreationRequest extends BaseRequest
{
    public function rules() {
        return [
            'firstname'        => 'required',
            'lastname'         => 'required',
            'phone'            => 'required',
            'email'            => 'required|email|unique:users',
            'password'         => 'required|same:confirm-password|min:6',
            'address'          => 'required|min:2|max:35',
        ];
    }

}
