<?php

namespace App\Http\API\Controllers\User;

use App\Base\Controller\BaseController;
use App\Base\Exception\BaseException;
use App\HOSLO\User\Requests\UserCreationRequest;
use App\HOSLO\User\Services\UserService;
use App\BO\Agent\v100\Services\WebsiteUserService;
use App\BO\Credit\v100\Models\CreditUser;
use App\BO\CustomerGroups\v100\Services\CustomerGroupService;
use Config;
use App\BO\CustomerGroups\v100\Models\CustomerGroup;
use Illuminate\Http\Request;
use Datatables;
use Auth;

class UserController extends BaseController
{

    public function __construct(UserService $userService)

    {
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userService->getUser();
        return response()->json(['success' => true, 'user' => $users]);
    }


     public function store(Request $request)
     {
         try {
             $newUser = $this->userService->createUser($request);

             if (isset($newUser)) {
                 return response()->json(['success' => true, 'message' => 'User Created Successfully']);
             } else{
                 return response()->json(['success' => false, 'message' => 'Something went wrong']);
             }

         } catch (BaseException $e) {

         }
     }

     public function update(Request $request,$id){
         try {
             $updateUser = $this->userService->updateUser($request,$id);

             if (isset($updateUser)) {
                 return response()->json(['success' => true,  'message' => 'User Updated Successfully']);
             } else{
                 return response()->json(['success' => false, 'message' => 'Something went wrong']);
             }
         } catch (BaseException $e) {

         }
     }

    public function delete($id)
    {
        try {
            $deleteUser = $this->userService->deleteUser($id);

            if (isset($deleteUser)) {
                return response()->json(['success' => true,  'message' => 'User Deleted Successfully']);
            } else{
                return response()->json(['success' => false, 'message' => 'Something went wrong']);
            }
        } catch (BaseException $e) {

        }
    }
}
