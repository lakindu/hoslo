<?php

namespace App\Http\Controllers\Agent\v100;

use App\Base\Controller\BaseController;
use App\Base\Exception\BaseException;
use App\BO\Agent\v100\Requests\AgentCreationRequest;
use App\BO\Agent\v100\Requests\AgentUpdateRequest;
use App\BO\Agent\v100\Services\AgentService;
use App\BO\Agent\v100\Services\WebsiteUserService;
use App\BO\Credit\v100\Models\CreditUser;
use App\BO\CustomerGroups\v100\Services\CustomerGroupService;
use Config;
use App\BO\CustomerGroups\v100\Models\CustomerGroup;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Str;
use Auth;
use App\BO\Agent\v100\Models\Agent;


class AgentController extends BaseController
{

    protected $agentService;
    protected $websiteUserService;

    public function __construct(AgentService $agentService, WebsiteUserService $websiteUserService)

    {
        parent::__construct(new Module());
        
        $this->agentService = $agentService;
        $this->websiteUserService = $websiteUserService;

        $this->middleware('revalidate');
        $this->middleware('permission:agent-list|agent-create|agent-edit', ['only' => ['index', 'store']]);
        $this->middleware('permission:agent-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:agent-edit', ['only' => ['edit', 'update']]);
    }

    public function index(Request $request)
    {
        $agents = $this->agentService->getAgents();
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.index', compact('agents'));
    }

    public function pendingIndex()
    {
        $agents = $this->agentService->getAgents();
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.pending-agent', compact('agents'));
    }

    public function create()
    {
        $user = auth()->user();
        $agentGroups = Config::get('constants.agent_groups');

        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.create');
    }

    public function edit($id)
    {
        $id = decrypt($id);

        $agent = $this->agentService->findAgentByIdNoWeb($id);
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.edit', compact('agent'));

    }

    public function pendingEdit($id)
    {
        $id = decrypt($id);

        $agent = $this->agentService->findAgentByIdNoWeb($id);
        $pending = true;
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.edit', compact('agent', 'pending'));
    }

    public function show($id)
    {
        $id = decrypt($id);
        $agent = $this->agentService->findAgentByIdNoWeb($id);
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.show', compact('agent'));
    }

    public function pendingShow($id)
    {
        $id = decrypt($id);
        $agent = $this->agentService->findAgentByIdNoWeb($id);
        $pending = true;
        return view($this->module->getModuleSlug() . "." . $this->module->getVersionPath() . '.show', compact('agent', 'pending'));
    }

    public function store(AgentCreationRequest $request)
    {
        try {
            $requestData = $request->request->all();
            $nwAgent = $this->agentService->createAgent($request);

            if (isset($nwAgent)) {
                return redirect()->route('agent.index')->with('success', 'Client created successfully');
            }
            return redirect()->route('agent.index')->with('info', 'Something went wrong.. Try again..');

        } catch (BaseException $e) {
            return redirect()->route('agent.index')->with('info', 'Something went wrong.. Try again..');
        }
    }
 
    public function checkAgent(Request $request)
    {
        $agent_phone = $request->agent_phone;
        $agent = $this->agentService->findAgentByPhone($agent_phone);

        if ($agent) {
            $data['agent_exist'] = true;
            $data['agent_id'] = $agent->id;
        } else {
            $data['agent_exist'] = false;
            $data['agent_id'] = null;
        }
        return $data;

    }

    public function addAgentToWebsite(Request $request)
    {
        $data['user_id'] = $request->agent_id;
        $data['website_id'] = $this->website->id;
        if ($this->websiteUserService->createWebsiteUser($data)) {
            return response()->json([
                "message" => "Success client added to your site."
            ]);
        } else {
            return response()->json([
                "error" => "Something went wrong."
            ]);
        }

    }
 
    public function searchCustomer(Request $request)
    {
        $agent = $this->agentService->searchCustomer($request);
        echo json_encode($agent);
        exit;
    }
}
