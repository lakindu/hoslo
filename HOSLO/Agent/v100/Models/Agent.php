<?php

namespace App\BO\Agent\v100\Models;

use App\Base\Exception\BaseException;
use App\Models\User;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Agent extends Model 
{
    
    protected $table = "users";

    protected $primaryKey = "id";
    protected $fillable = [
        'first_name',
        'last_name',
        'name',
        'email',
        'phone',
        'password',
        'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
