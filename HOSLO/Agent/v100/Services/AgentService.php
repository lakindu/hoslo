<?php
/**
 * LocationService
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev4
 * @Date       2020-01-20
 * @Time       4:13 PM
 */

namespace App\BO\Agent\v100\Services;

use App\Base\Exception\BaseException;
use App\Base\Services\BaseService; 
use App\BO\Agent\v100\Repositories\AgentRepository; 
use App\BO\Agent\v100\Transformations\AgentTransformable; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class AgentService extends BaseService
{
    protected $agentRepo;
    protected $websiteUserRepo;

    use AgentTransformable;

    public function __construct(AgentRepository $agentRepo)
    {
        $this->agentRepo = $agentRepo;
        parent::__construct();
    }

    public function loginWeb(Request $request)
    {
        try {

            $agentData = $this->agentRepo->loginWeb($request);
            if ($agentData) {
                Auth::loginUsingId($agentData->id);
                Auth::check();
                return $this->transformAgentDataWithSalesChannel($agentData);
            }
            return false;

        } catch (BaseException $e) {
            throw $e;
        } catch (\Exception $e) {
            new BaseException(1005);
        }
    }
 
    public function findAgentByIdNoWeb($id)
    {
        try {
            $agent = $this->agentRepo->findAgentByIdNoWeb($id);
            return $agent;

        } catch (BaseException $e) {
            throw $e;
        }
    }

    public function findAgentById($id)
    {
        try {
            $agent = $this->agentRepo->findAgentById($id);
            return $agent;

        } catch (BaseException $e) {
            throw $e;
        }
    }

    public function findAgentByEmail($id)
    {
        try {
            $agent = $this->agentRepo->findAgentByEmail($id);
            return $agent;
        } catch (BaseException $e) {
            throw $e;
        }
    }

    public function findAgentByPhone($id)
    {
        try {
            $agent = $this->agentRepo->findAgentByPhone($id);
            return $agent;
        } catch (BaseException $e) {
            throw $e;
        }
    }
 
}
