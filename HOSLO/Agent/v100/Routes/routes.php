<?php

Route::prefix('agent')->namespace('Agent\\'.$version)->group(function(){
    Route::get('get-agent-data', [\App\Http\Controllers\Agent\v100\AgentController::class, 'getAgentData'])->name('agent.data');
    Route::get('get-pending-agent-data', [\App\Http\Controllers\Agent\v100\AgentController::class, 'getPendingAgentData'])->name('pending-agent.data');
    Route::GET('/',             [\App\Http\Controllers\Agent\v100\AgentController::class, 'index'])->name('agent.index');
    Route::GET('index',         [\App\Http\Controllers\Agent\v100\AgentController::class, 'index'])->name('agent.index');
    Route::GET('pending/index',  [\App\Http\Controllers\Agent\v100\AgentController::class, 'pendingIndex'])->name('pending-agent.index');
    Route::GET('show/{id}',     [\App\Http\Controllers\Agent\v100\AgentController::class, 'show'])->name('agent.show');
    Route::GET('pending/show/{id}', [\App\Http\Controllers\Agent\v100\AgentController::class, 'pendingShow'])->name('pending-agent.show');
    Route::GET('edit/{id}',     [\App\Http\Controllers\Agent\v100\AgentController::class, 'edit'])->name('agent.edit');
    Route::GET('pending/edit/{id}',     [\App\Http\Controllers\Agent\v100\AgentController::class, 'pendingEdit'])->name('pending-agent.edit');
    Route::GET('create',        [\App\Http\Controllers\Agent\v100\AgentController::class, 'create'])->name('agent.create');
    Route::POST('store',        [\App\Http\Controllers\Agent\v100\AgentController::class, 'store'])->name('agent.store');
    Route::PUT('update/{id}',   [\App\Http\Controllers\Agent\v100\AgentController::class, 'update'])->name('agent.update');
    Route::GET('check-agent',   [\App\Http\Controllers\Agent\v100\AgentController::class, 'checkAgent'])->name('agent.check');
    Route::POST('add-agent-to-website',   [\App\Http\Controllers\Agent\v100\AgentController::class, 'addAgentToWebsite'])->name('agent.addtosite');
});
Route::get('customer/search',[\App\Http\Controllers\Agent\v100\AgentController::class, 'searchCustomer'])->name('customer.search');
Route::post('customer/search',[\App\Http\Controllers\Agent\v100\AgentController::class, 'searchCustomer'])->name('customer.search');
