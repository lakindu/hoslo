<?php


namespace App\BO\Agent\v100\Requests;

use App\Http\Requests\BaseRequest;
use Config;

class AgentCreationRequest extends BaseRequest
{
    public function rules() {
        return [
            'firstname'              => 'required|max:'.Config::get('constants.name_character_limit'),
            // 'phone'             => 'required|unique:users',
            // 'email'             => 'required|email|unique:users',
            // 'password'          => 'required|same:confirm-password|min:6',
            // 'confirm-password'  => 'required|min:6',
            // 'address_1'         => 'required|min:2|max:35',
            // 'address_2'         => 'nullable|min:2|max:35',
            // 'city'              =>'required|max:35',
        ];
    }

}
