<?php


namespace HOSLO\User\Repositories\Interfaces;
 
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    // public function login(Request $request);

    public function getUser();

    // public function register(Request $request);
}
