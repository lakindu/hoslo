<?php
/**
 * LocationTransformable
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev3
 * Date: 20/01/2020
 * Time: 10:50 AM
 */

namespace HOSLO\User\Transformations;

use HOSLO\User\Models\User;
use App\BO\Cart\v100\Models\Cart;
use App\BO\Cart\v100\Models\CartOrder;
use App\BO\Cart\v100\Services\CartOrderService;
use App\BO\Company\v100\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Base\Exception\BaseException;
use Illuminate\Support\Facades\Config;
use Auth;

trait UserTransformable
{

    public function transformActiveAgents(Object $agentList): Array
    {
        $agents = [];
        foreach ($agentList as $agent) {
            $agents[$agent->id] = $agent->name;
        }
        return $agents;
    }

    public function transformAgentData($agentData = [])
    {
        try {


            $cartsData = Cart::where(["user_id" => auth()->id()])->orderBy('id', 'DESC')->first();
            $carts = CartOrder::where(["user_id" => auth()->id(), "cart_id" => $cartsData->id ?? ""])->orderBy('id', 'DESC')->first();
            //  $carts = $carts->first();

            $cart_id = 0;
            if (!$carts) {
                $cart_id = ($cartsData->id ?? 0);
            }

            return [
                    "name" => $agentData->name,
                    "first_name" => $agentData->first_name,
                    "last_name" => $agentData->last_name,
                    "email" => $agentData->email,
                    "phone" => $agentData->phone,
                    "organization_name" => $agentData->organization,
                    "organization" => $agentData->organization,
                    "address_1" => $agentData->address_1,
                    "address_2" => $agentData->address_2,
                    "city" => $agentData->city,
                    "gender" => $agentData->gender,
                    "country" => $agentData->country_code,
                    "is_verified" => (int)$agentData->is_verified,
                    "firebase_token" => $agentData->fire_base_token,
                    "is_social_account" => (bool)($agentData->social_account != ""),
                    "coordinates" => ($agentData->coordinates ?? ""),
                    "cart_id" => $cart_id

                ] + ((isset($agentData->accessToken)) ? ["accessToken" => (string)$agentData->accessToken] : []);
        } catch (\Exception $e) {
            dd($e);
            throw new BaseException(9006);
        }
    }


    public function transformAgentDataWithSalesChannel($agentData = [])
    {
        try {

            $cartsData = Cart::where(["user_id" => $agentData->id])->orderBy('id', 'DESC')->first();
            $carts = CartOrder::where(["user_id" => $agentData->id, "cart_id" => $cartsData->id ?? ""])->orderBy('id', 'DESC')->first();
            //  $carts = $carts->first();

            $cart_id = 0;
            if (!$carts) {
                $cart_id = ($cartsData->id ?? 0);
            }
            return [
                    "user_id" => $agentData->id,
                    "name" => $agentData->name,
                    "first_name" => $agentData->first_name,
                    "last_name" => $agentData->last_name,
                    "email" => $agentData->email,
                    "phone" => $agentData->phone,
                    "organization_name" => $agentData->organization_name,
                    "address_1" => $agentData->address_1,
                    "address_2" => $agentData->address_2,
                    "city" => $agentData->city,
                    "gender" => $agentData->gender,
                    "country" => $agentData->country_code,
                    "is_verified" => (int)$agentData->is_verified,
                    "firebase_token" => $agentData->fire_base_token,
                    "is_social_account" => (bool)($agentData->social_account != ""),
                    "coordinates" => ($agentData->coordinates ?? ""),
                    "cart_id" => $cart_id

                ] + ((isset($agentData->accessToken)) ? ["accessToken" => (string)$agentData->accessToken] : []);
        } catch (\Exception $e) {
            dd($e);
            throw new BaseException(9006);
        }
    }

}
