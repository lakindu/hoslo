<?php
/**
 * LocationService
 *
 * @package    BO
 * @subpackage Location
 * @author     Dev4
 * @Date       2020-01-20
 * @Time       4:13 PM
 */

namespace HOSLO\User\Services;

use App\Base\Exception\BaseException;
use App\Base\Services\BaseService; 
use HOSLO\User\Repositories\UserReposirory; 
use HOSLO\User\Transformations\UserTransformable; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class UserService extends BaseService
{
    protected $userRepo;
    protected $websiteUserRepo;

    use UserTransformable;

    public function __construct(UserReposirory $userRepo)
    {
        $this->userRepo = $userRepo;
        parent::__construct();
    }

    // public function loginWeb(Request $request)
    // {
    //     try {

    //         $agentData = $this->agentRepo->loginWeb($request);
    //         if ($agentData) {
    //             Auth::loginUsingId($agentData->id);
    //             Auth::check();
    //             return $this->transformAgentDataWithSalesChannel($agentData);
    //         }
    //         return false;

    //     } catch (BaseException $e) {
    //         throw $e;
    //     } catch (\Exception $e) {
    //         new BaseException(1005);
    //     }
    // }
 
    // public function findAgentByIdNoWeb($id)
    // {
    //     try {
    //         $agent = $this->agentRepo->findAgentByIdNoWeb($id);
    //         return $agent;

    //     } catch (BaseException $e) {
    //         throw $e;
    //     }
    // }

    // public function findAgentById($id)
    // {
    //     try {
    //         $agent = $this->agentRepo->findAgentById($id);
    //         return $agent;

    //     } catch (BaseException $e) {
    //         throw $e;
    //     }
    // }

    // public function findAgentByEmail($id)
    // {
    //     try {
    //         $agent = $this->agentRepo->findAgentByEmail($id);
    //         return $agent;
    //     } catch (BaseException $e) {
    //         throw $e;
    //     }
    // }

    public function getUser()
    {
        try {
            $agent = $this->userRepo->getUser();
            return $agent;
        } catch (BaseException $e) {
            throw $e;
        }
    }
 
}
