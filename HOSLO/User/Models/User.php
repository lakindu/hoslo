<?php

namespace HOSLO\User\Models;

use App\Base\Exception\BaseException;
use HOSLO\User\Models\User;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Agent extends Model
{

    protected $table = "users";

    protected $primaryKey = "id";
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
        'address',
        'username',
        'password',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
